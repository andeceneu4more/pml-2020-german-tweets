from torch.utils.data import Dataset
import torch
import pandas as pd

class GermanTweetsDataset(Dataset):
    """ dataset class used just for training
    """
    def __init__(self, csv_path):
        super().__init__()
        self.path = csv_path
        self.df = pd.read_csv(csv_path)

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        row = self.df.iloc[idx]
        text = [int(x) for x in row['tweet'].split(' ')]
        text = torch.tensor(text)
        position = int(row['tweet_offset'])
        position = torch.tensor(position)
        
        latitude = row['latitude']
        longitude = row['longitude']
        labels = torch.tensor([latitude, longitude])
        
        return text, position, labels