import pandas as pd
import numpy as np
import re
import torch
import argparse
import json

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.snowball import GermanStemmer

from gensim.models import Word2Vec
import emot

from __init__ import word2vec_path, embedder_path, scalling_params_path, \
                     training_data_path, validation_data_path, testing_data_path, \
                     training_dataset, validation_dataset, testing_dataset

special_chars_re = r"[#\?\.$%_\[\]()+-:*\"!„“]"
single_letters_re = r'\s\w\s'
spaces_re = r"\s+"

def convert_emoticons(text):
    """ turns the emoji and the emoticons into description between a pair of :
    """
    emojis = emot.emoji(text)
    for value, meaning in zip(emojis['value'], emojis['mean']):
        text = re.sub(value, f' {meaning} ', text, flags=re.I)
    
    emoticons = emot.EMOTICONS
    for emoticon in emoticons:
        text = re.sub(u'('+emoticon+')', \
                      "_".join(emoticons[emoticon].replace(",","").split()), \
                      text)
    return text

def char_cleaning(doc_str):
    """ erases special characters, apostrophe and places a user_tag word for any @
        also converts the emoticons and deletes single letter words
    """
    doc_str = re.sub(special_chars_re, ' ', doc_str, flags=re.I)
    doc_str = re.sub(r'‘', '_ ' , doc_str, flags=re.I)
    doc_str = re.sub(r'@\w+', ':user_tag:', doc_str, flags=re.I)
    doc_str = convert_emoticons(doc_str)
    
    # delete single letter words
    while re.search(single_letters_re, doc_str):
        doc_str = re.sub(single_letters_re, ' ' , doc_str, flags=re.I)
    doc_str = re.sub(spaces_re, ' ', doc_str)
    return doc_str.strip().lower()

def filter_stopwords(tweets_processed):
    """ gets rid of the stopwords using nltk 
    """
    german_stop_words = stopwords.words('german')

    filtered_tweets = []
    for tweet in tweets_processed:
        word_tokens = word_tokenize(tweet)
        filtered_tweet = [w for w in word_tokens if not w in german_stop_words]
        filtered_tweets.append(filtered_tweet)
    return filtered_tweets

def get_word_embedding(stemmed_tweets, training, config):
    """ trains a contextual word embedding which will not be further retrained
    """
    if training:
        german2vec = Word2Vec(
            sentences=stemmed_tweets, 
            sg=config["skip_grams"],
            hs=config["hierarchical_softmax"],
            size=config["d_model"], 
            window=config["embedding_window"],
            min_count=config["min_count"],
            iter=config["embedding_iterations"],
            workers=4
        )
        german2vec.save(word2vec_path)
    else:
        german2vec = Word2Vec.load(word2vec_path)
    return german2vec

def get_encoded_texts(german2vec, stemmed_tweets, seq_len):
    """ based on the vocabulary returned from word2vec, it computes 
        the embeddings list and saves it as torch.tensor;
        it also pads the incomplete sequences with a 0 hardcoded vector (blanc)
    """
    word_to_id = {}
    embedded_voc = []
    for idx, word in enumerate(german2vec.wv.vocab.keys()):
        word_to_id[word] = idx

        embedding = np.copy(german2vec.wv[word])
        embedding = torch.from_numpy(embedding)
        embedding = embedding.unsqueeze(0)
        embedded_voc.append(embedding)
        
        print(f'Preparation progress {idx}/{len(german2vec.wv.vocab)}', end='\r')
    print()

    # Add blanc words for completing the sequence
    blanc_id = len(german2vec.wv.vocab)
    embedded_voc.append(torch.zeros_like(embedding))

    embedded_voc = torch.cat(embedded_voc)
    torch.save(embedded_voc, embedder_path)

    tweets = []
    for tweet in stemmed_tweets:
        tweet = [word_to_id[word] for word in tweet if word in word_to_id.keys()]
        if len(tweet) < seq_len:
            blanc_spaces = (seq_len - len(tweet))
            tweet += blanc_spaces * [blanc_id]
        tweets.append(tweet)
    return tweets

def preprocess_tweets(dataset, training, config):
    """ performs the cleaning, stopwords removal, stemming, embedding;
        the embeddings return a vocabulary which is used for filtering the original text
        and then applying tf_idf algorithm; also, it appends the value 0 for the blanc word
    """
    tweets = list(dataset["tweet"])
    tweets_processed = list(map(char_cleaning, tweets))
    filtered_tweets = filter_stopwords(tweets_processed)

    stemmer = GermanStemmer()
    stemmed_tweets = [list(map(stemmer.stem, tweet)) for tweet in filtered_tweets]
    german2vec = get_word_embedding(stemmed_tweets, training, config)
    dataset["tweet"] = get_encoded_texts(german2vec, stemmed_tweets, \
                                         config['seq_len'])

    return dataset

def preprocess_coordinates(dataset, training, scalling_coordinates):
    """ performs the scalling of the gps coordinates or just the translation of the interval
    """
    scalling_cols = ['latitude', 'longitude']
    scalling_param_names = []
    scalling_param_values = []

    if training:
        for coordonate in scalling_cols:
            if scalling_coordinates:
                scalling_param_names.append(f'{coordonate}_min')
                scalling_param_values.append(dataset[coordonate].min())
                
                scalling_param_names.append(f'{coordonate}_max')
                scalling_param_values.append(dataset[coordonate].max())
            else:
                scalling_param_names.append(f'{coordonate}_mean')
                scalling_param_values.append(dataset[coordonate].mean())
    
        scalling_params = np.array(list(zip(scalling_param_names, scalling_param_values)))
        np.save(scalling_params_path, scalling_params)
    else:
        scalling_params = np.load(scalling_params_path)
        scalling_param_names = list(scalling_params[:, 0])
        scalling_param_values = list(map(float, scalling_params[:, 1]))

    dataset[scalling_param_names] = scalling_param_values

    for coordonate in scalling_cols:
        if scalling_coordinates:
            dataset[coordonate] = (dataset[coordonate] - dataset[f'{coordonate}_min']) / \
                                  (dataset[f'{coordonate}_max'] - dataset[f'{coordonate}_min'])
        else:
            dataset[coordonate] -= dataset[f'{coordonate}_mean']
    for col in scalling_param_names:
        dataset.pop(col)

    return dataset

def get_sequence_line(tweet_id, tweet_row, sequence, j, testing):
    """ returns a pandas series containing a line in the final dataframe;
        contains the sequence, tf-idf etc. 
    """
    line_config = {
        'tweet_id': int(tweet_id), 
        'tweet': " ".join(str(w_id) for w_id in sequence),
        'tweet_offset': j
    }
    if not testing:
        line_config.update({
            'latitude': tweet_row["latitude"], 
            'longitude': tweet_row["longitude"]
        })
                
    return pd.Series(data=line_config, name=f'{tweet_id}-{j}')

def get_sequence_dataset(dataset, seq_len, seq_stride, training, testing):
    """ turns the dataset into sequences having a length and a stride
    """
    seq_df = pd.DataFrame()

    try:
        for i in range(len(dataset)):
            tweet_row = dataset.iloc[i]
            tweet_id = dataset.index[i]
            
            tweet = tweet_row['tweet']
            for j in range(0, len(tweet) - seq_len + 1, seq_stride):
                sequence = tweet[j: j + seq_len]
                
                seq_line = get_sequence_line(tweet_id, tweet_row, sequence, j, testing)
                seq_df = seq_df.append(seq_line)
                
                print(f'Preparation progress {i}/{len(dataset)}', end='\r')
    except KeyboardInterrupt:
        print('\nIntreruption')
    finally:
        print('\nPreparation done')
        if training:
            seq_df.to_csv(training_data_path)
        elif testing:
            seq_df.to_csv(testing_data_path)
        else:
            seq_df.to_csv(validation_data_path)
    return seq_df

def export_sequenced_dataset(dataset_path, config, training=True, testing=False):
    """ performs the proper operations for the dataset:
        reading, the tweets, the coordinates, transformation into sequences
    """
    col_names = ['tweet-id', 'latitude','longitude', 'tweet']
    if testing:
        col_names = [col_names[0], col_names[-1]]

    dataset = pd.read_csv(dataset_path, names=col_names)
    dataset = dataset.set_index('tweet-id', drop=True)

    seq_len = config['seq_len']
    seq_stride = config['seq_stride']

    dataset = preprocess_tweets(dataset, training, config)
    if not testing:
        dataset = preprocess_coordinates(dataset, training, config["scalling_coordinates"])
    get_sequence_dataset(dataset, seq_len, seq_stride, training, testing)

def main(input_json):
    """ reads the json from calling master and preprocess the 3 datasets having the path in __init__
    """
    with open(input_json, 'r') as fin:
        config = json.load(fin)

    export_sequenced_dataset(training_dataset, config)
    export_sequenced_dataset(validation_dataset, config, training=False)
    export_sequenced_dataset(testing_dataset, config, training=False, testing=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-json', dest='input_json', type=str, \
                        help='The file name of the json from where we\'ll process the data')
    args = parser.parse_args()
    args = vars(args)
    main(args['input_json'])