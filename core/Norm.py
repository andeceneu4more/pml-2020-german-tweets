import torch
import torch.nn as nn
import torch.nn.functional as F

class Norm(nn.Module):
    """ performs Layer Normalization
    """
    def __init__(self, d_model, eps=1e-6):
        super().__init__()
    
        self.size = d_model
        self.alpha = nn.Parameter(torch.ones(self.size))
        self.beta = nn.Parameter(torch.zeros(self.size))
        self.eps = eps
    def forward(self, x):
        x_mean = x.mean(dim=-1, keepdim=True)
        x_std = x.std(dim=-1, keepdim=True) + self.eps
        
        norm = self.alpha * (x - x_mean) / x_std + self.beta
        return norm