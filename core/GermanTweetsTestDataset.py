from torch.utils.data import Dataset
import torch
import pandas as pd

class GermanTweetsTestDataset(Dataset):
    """ dataset class used just for submition
    """
    def __init__(self, csv_path):
        super().__init__()
        self.path = csv_path
        self.grouped_df = pd.read_csv(csv_path).groupby(["tweet_id"])
        self.tweet_ids = list(self.grouped_df.groups.keys())

    def __len__(self):
        return len(self.tweet_ids)

    def __getitem__(self, idx):
        tweet_id = self.tweet_ids[idx]
        tweet_sequences = self.grouped_df.get_group(tweet_id)

        texts = []
        for row in tweet_sequences["tweet"].tolist():
            text = torch.tensor([int(x) for x in row.split(' ')])
            text = text.unsqueeze(0)
            texts.append(text)
        texts = torch.cat(texts)

        positions = tweet_sequences["tweet_offset"].tolist()
        positions = list(map(int, positions))
        positions = torch.tensor(positions)
        
        return tweet_id, texts, positions