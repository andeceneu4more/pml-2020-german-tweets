from __init__ import get_localizer, training_data_path, validation_data_path, get_device

from GermanTweetsDataset import GermanTweetsDataset
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.optim import Adam

import torch
import torch.nn as nn

import numpy as np
import pandas as pd
import os
import json
import argparse

def get_lr(optim):
    """ extracts the learning rate from the optimizer
    """
    for param_group in optim.param_groups:
        return param_group['lr']

def training(dataloaders, localizer, optim, lr_scheduler, epochs, device, best_loss, exp_dir):
    """ performs the full training process with printing results and advancing with the lr_scheduler
    the try catch block saves the training scores inside the epochs, altough interupted
    """
    MAE = nn.L1Loss(reduction='mean')
    print_frequency = 10

    tensorboard_dir = os.path.join(exp_dir, 'runs/')
    writer = SummaryWriter(tensorboard_dir)
    print(f'Tensorboard is recording into folder: {tensorboard_dir}\n')

    try:
        results = []
        for epoch in range(epochs):
            for phase in ['train', 'test']:
                epoch_losses = []

                if phase == 'train':
                    # training mode
                    localizer.train()
                else:
                    # evaluate mode
                    localizer.eval()

                for i, batch in enumerate(dataloaders[phase]):
                    sequences, positions, labels = batch
                    sequences = sequences.to(device)
                    positions = positions.to(device)
                    labels = labels.to(device)

                    optim.zero_grad()
                    with torch.set_grad_enabled(phase == 'train'):
                        output = localizer(sequences, positions)
                        loss = MAE(output, labels) 

                        if phase == 'train':
                            loss.backward()
                            optim.step()

                    epoch_losses.append(loss.item())
                    average_loss = np.mean(epoch_losses)
                    lr = get_lr(optim)

                    if (i + 1) % print_frequency == 0:
                        loading_percentage = int(100 * (i+1) / len(dataloaders[phase]))
                        print(f'{phase}ing epoch {epoch}, iter = {i+1}/{len(dataloaders[phase])} ' + \
                            f'({loading_percentage}%), loss = {loss}, average_loss = {average_loss} ' + \
                            f'learning rate = {lr}', end='\r')
                        
                        

                if phase == 'test' and average_loss < best_loss:
                    best_loss = average_loss
                    
                    torch.save({
                            'model': localizer.state_dict(),
                            'optimizer': optim.state_dict(),
                            'lr_scheduler': lr_scheduler.state_dict(),
                            'epoch': epoch,
                            'validation_loss': best_loss
                        }, os.path.join(exp_dir, 'best.pth'))
                    
                if phase == 'train':
                    metric_results = {
                        'train_loss': average_loss
                    }

                    writer.add_scalar('Train/Loss', average_loss, epoch)
                    writer.flush()

                if phase == 'test':
                    val_results = {
                        'val_loss': average_loss
                    }

                    metric_results.update(val_results)
                    results.append(metric_results)

                    writer.add_scalar('Test/Loss', average_loss, epoch)
                    writer.flush()

                    try:
                        lr_scheduler.step()
                    except:
                        lr_scheduler.step(average_loss)
                
                print()
    except Exception as ex:
        print(ex)
    finally:
        results = pd.DataFrame(results)
        history_path = os.path.join(exp_dir, 'history.csv')
        results.to_csv(history_path)
        return best_loss

def load_weights(model, path, optimizer=None, lr_scheduler=None):
    """ loads the customized dict of weights
    """
    sd = torch.load(path)
    model.load_state_dict(sd['model'])
    if optimizer:
        optimizer.load_state_dict(sd['optimizer'])
    if lr_scheduler:
        lr_scheduler.load_state_dict(sd['lr_scheduler'])
    epoch = sd['epoch']
    validation_loss = sd['validation_loss']

    print(f'Loaded model from epoch {epoch + 1} with validation loss = {validation_loss} \n')
    return validation_loss
 
def get_exp_dir(config, input_json):
    """ returns a new folder to export the model weigths and configuration
    """
    exp_dir = f'../logs/transformer_{config["seq_len"]}_{config["seq_stride"]}' + \
              f'{config["heads"]}_{config["d_model"]}'

    if config['fine_tune']:
        exp_dir += '_fine_tune'

    os.makedirs(exp_dir, exist_ok=True)

    experiments = [d for d in os.listdir(exp_dir) if os.path.isdir(os.path.join(exp_dir, d))]
    experiments = set(map(int, experiments))
    if len(experiments) > 0:
        possible_experiments = set(range(1, max(experiments) + 2))
        experiment_id = min(possible_experiments - experiments)
    else:
        experiment_id = 1

    exp_dir = os.path.join(exp_dir, str(experiment_id))
    os.makedirs(exp_dir, exist_ok=True)

    config_path = os.path.join(exp_dir, 'config.json')
    with open(config_path, 'w') as fout:
        json.dump(config, fout)

    exp_weights = os.path.join(exp_dir, 'best.pth')
    config["experiment_weights"] = exp_weights
    with open(input_json, 'w') as fout:
        json.dump(config, fout)
    return exp_dir

def main(input_json):
    with open(input_json, 'r') as fin:
        config = json.load(fin)

    batch_size = config["batch_size"]
    epochs = config["epochs"]
    device = get_device()

    localizer = get_localizer(config)
    localizer = localizer.to(device)
    training_dataset = GermanTweetsDataset(training_data_path)
    training_dataloader = DataLoader(training_dataset, batch_size=batch_size, \
                                     shuffle=True, num_workers=4)

    validation_dataset = GermanTweetsDataset(validation_data_path)
    validation_dataloader = DataLoader(validation_dataset, batch_size=batch_size, \
                                     shuffle=False, num_workers=4)

    dataloaders = {
        'train': training_dataloader,
        'test': validation_dataloader
    }

    if config["optimizer"] == "adam":
        optim = Adam(localizer.parameters(), lr=0.0001, betas=(0.9, 0.98), eps=1e-9)
    lr_scheduler = ReduceLROnPlateau(optim,
                                     factor = 0.1,
                                     patience = 3,
                                     mode = 'min')
    best_loss = 500
    exp_dir = get_exp_dir(config, input_json)
    best_loss = training(dataloaders, localizer, optim, lr_scheduler, epochs, device, best_loss, exp_dir)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-json', dest='input_json', type=str, \
                        help='The file name of the json from where we\'ll process the data')
    args = parser.parse_args()
    args = vars(args)
    main(args['input_json'])