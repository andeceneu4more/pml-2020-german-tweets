import json
import subprocess

config_submission_1 = {
    "batch_size": 64,
    "seq_len": 15,
    "seq_stride": 2,
    "d_model": 100,
    "heads": 2,
    "hidden_units": 256,
    "n_encoding_layers": 1,
    "epochs": 25,
    "fine_tune": False,
    "optimizer": "adam",
    "target_size": 2,
    "skip_grams": 1,
    "hierarchical_softmax": 1,
    "embedding_window": 5,
    "min_count": 3,
    "embedding_iterations": 5,
    "scalling_coordinates": True,
    "choose_atention": "last" 
}

config_submission_2 = {
    "batch_size": 64,
    "seq_len": 15,
    "seq_stride": 2,
    "d_model": 300,
    "heads": 6,
    "hidden_units": 512,
    "n_encoding_layers": 3,
    "epochs": 20,
    "fine_tune": False,
    "optimizer": "adam",
    "target_size": 2,
    "skip_grams": 0,
    "hierarchical_softmax": 1,
    "embedding_window": 5,
    "min_count": 3,
    "embedding_iterations": 5,
    "scalling_coordinates": True,
    "choose_atention": "last"
}

config_submission_3 = {
    "batch_size": 128,
    "seq_len": 25,
    "seq_stride": 5,
    "d_model": 300,
    "heads": 6,
    "hidden_units": 512,
    "n_encoding_layers": 5,
    "epochs": 20,
    "fine_tune": False,
    "optimizer": "adam",
    "target_size": 2,
    "skip_grams": 1,
    "hierarchical_softmax": 1,
    "embedding_window": 9,
    "min_count": 3,
    "embedding_iterations": 10,
    "scalling_coordinates": True,
    "choose_atention": "last"
}
# Best weights 
# D:\projects\FMI Master\Practical ML\pml-2020-german-tweets\logs\transformer_25_56_300\1\best.pth

json_path = '../running_config.json'
run_env = 'python'
preprocess_script = 'preprocess.py'
training_script = 'train.py'
inference_script = 'inference.py'

def main():
    with open(json_path, 'w') as fout:
        json.dump(config_submission_2, fout)

    preprocess_cmd = f'{run_env} {preprocess_script} --input-json {json_path}'
    train_cmd = f'{run_env} {training_script} --input-json {json_path}'
    inference_cmd = f'{run_env} {inference_script} --input-json {json_path}'

    subprocess.check_call(preprocess_cmd, shell = True)
    subprocess.check_call(train_cmd, shell = True)
    subprocess.check_call(inference_cmd, shell = True)


if __name__ == "__main__": 
    main()