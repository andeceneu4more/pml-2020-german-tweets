import torch
import os

import numpy as np
from Localizer import Localizer

training_dataset = "training.txt"
training_dataset = os.path.join('..', training_dataset)

validation_dataset = "validation.txt"
validation_dataset = os.path.join('..', validation_dataset)

testing_dataset = "test.txt"
testing_dataset = os.path.join('..', testing_dataset)

training_data_path = 'training-stemmed-seq.txt'
training_data_path = os.path.join('..', training_data_path)

validation_data_path = 'validation-stemmed-seq.txt'
validation_data_path = os.path.join('..', validation_data_path)

testing_data_path = 'testing-stemmed-seq.txt'
testing_data_path = os.path.join('..', testing_data_path)

submission_path = 'submission.txt'
submission_path = os.path.join('..', submission_path)

embedder_path = 'embedded-trains.pkl'
embedder_path = os.path.join('..', embedder_path)

word2vec_path = 'german2vec.model'
word2vec_path = os.path.join('..', word2vec_path)

scalling_params_path = 'scalling_params.npy'
scalling_params_path = os.path.join('..', scalling_params_path)

hyper_graph_path = 'hyper-graph'
hyper_graph_path = os.path.join('..', hyper_graph_path)

def get_localizer(config):
    return Localizer(config['n_encoding_layers'], \
                     embedder_path, \
                     config['d_model'], \
                     config['heads'], \
                     config['hidden_units'], \
                     config['target_size'])

random_seed = 42
def get_device():
    np.random.seed(random_seed)
    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.manual_seed(random_seed)
        torch.cuda.manual_seed_all(random_seed)
        device = torch.device('cuda') 
    else:
        torch.manual_seed(random_seed)
        device = torch.device('cpu')
    return device
