import torch.nn as nn
import torch.nn.functional as F

from Norm import Norm
from MultiHeadAttention import MultiHeadAttention

class EncoderLayer(nn.Module):
    """ the greatest rectangle from the architecture figure - transformer layer
    """
    def __init__(self, d_model, heads, hidden_units=256, dropout_1=0.1, dropout_2=0.1, dropout_3=0.1):
        super().__init__()
        self.norm_1 = Norm(d_model)
        self.norm_2 = Norm(d_model)
        self.attn = MultiHeadAttention(heads, d_model)
        
        self.linear_1 = nn.Linear(d_model, hidden_units)
        self.linear_2 = nn.Linear(hidden_units, d_model)
        
        self.dropout_1 = nn.Dropout(dropout_1)
        self.dropout_2 = nn.Dropout(dropout_2)
        self.dropout_3 = nn.Dropout(dropout_3)
        
    def forward(self, x, mask=None):
        x_normed = self.norm_1(x)
        x_attention = self.attn(x_normed, x_normed , x_normed, mask)
        x = x + self.dropout_1(x_attention)
        
        x_normed = self.norm_2(x)
        x_normed = self.linear_1(x_normed)
        x_normed = self.dropout_2(F.relu(x_normed))
        x_normed = self.linear_2(x_normed)
        x = x + self.dropout_3(x_normed)
        return x