import torch
import torch.nn as nn
import numpy as np

class PositionalEncoder(nn.Module):
    """ computes the positional encoder and stores inside the class
    """
    def __init__(self, d_model, max_seq_len=250):
        super().__init__()
        self.d_model = d_model
        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        
        pe = torch.zeros(max_seq_len, d_model)
        for pos in range(max_seq_len):
            for i in range(0, d_model, 2):
                pe[pos, i] = np.sin(pos / (10000 ** ((2 * i) / d_model)))
                pe[pos, i + 1] = np.cos(pos / (10000 ** ((2 * (i + 1)) /d_model)))
        
        self.pe = pe.to(self.device)
    
    def get_positionals(self, positions, seq_len):
        positionals = []
        for pos in positions:
            positional = self.pe[pos: pos + seq_len, :] \
                             .clone() \
                             .detach() \
                             .requires_grad_(False)
            positional = positional.unsqueeze(0)
            positionals.append(positional)
        positionals = torch.cat(positionals)
        return positionals
    
    
    def forward(self, x, positions):
        x = x * np.sqrt(self.d_model)
        seq_len = x.size(1)
        
        positionals = self.get_positionals(positions, seq_len)
        x = x + positionals
        return x