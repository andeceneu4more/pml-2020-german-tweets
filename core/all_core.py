######### __init__.py


import torch
import os

import numpy as np
from Localizer import Localizer

training_dataset = "training.txt"
training_dataset = os.path.join('..', training_dataset)

validation_dataset = "validation.txt"
validation_dataset = os.path.join('..', validation_dataset)

testing_dataset = "test.txt"
testing_dataset = os.path.join('..', testing_dataset)

training_data_path = 'training-stemmed-seq.txt'
training_data_path = os.path.join('..', training_data_path)

validation_data_path = 'validation-stemmed-seq.txt'
validation_data_path = os.path.join('..', validation_data_path)

testing_data_path = 'testing-stemmed-seq.txt'
testing_data_path = os.path.join('..', testing_data_path)

submission_path = 'submission.txt'
submission_path = os.path.join('..', submission_path)

embedder_path = 'embedded-trains.pkl'
embedder_path = os.path.join('..', embedder_path)

word2vec_path = 'german2vec.model'
word2vec_path = os.path.join('..', word2vec_path)

scalling_params_path = 'scalling_params.npy'
scalling_params_path = os.path.join('..', scalling_params_path)

hyper_graph_path = 'hyper-graph'
hyper_graph_path = os.path.join('..', hyper_graph_path)

def get_localizer(config):
    return Localizer(config['n_encoding_layers'], \
                     embedder_path, \
                     config['d_model'], \
                     config['heads'], \
                     config['hidden_units'], \
                     config['target_size'])

random_seed = 42
def get_device():
    np.random.seed(random_seed)
    if torch.cuda.is_available():
        torch.cuda.empty_cache()
        torch.cuda.manual_seed(random_seed)
        torch.cuda.manual_seed_all(random_seed)
        device = torch.device('cuda') 
    else:
        torch.manual_seed(random_seed)
        device = torch.device('cpu')
    return device


######### calling_master.py

import json
import subprocess

config_submission_1 = {
    "batch_size": 64,
    "seq_len": 15,
    "seq_stride": 2,
    "d_model": 100,
    "heads": 2,
    "hidden_units": 256,
    "n_encoding_layers": 1,
    "epochs": 25,
    "fine_tune": False,
    "optimizer": "adam",
    "target_size": 2,
    "skip_grams": 1,
    "hierarchical_softmax": 1,
    "embedding_window": 5,
    "min_count": 3,
    "embedding_iterations": 5,
    "scalling_coordinates": True,
    "choose_atention": "last" 
}

config_submission_2 = {
    "batch_size": 64,
    "seq_len": 15,
    "seq_stride": 2,
    "d_model": 300,
    "heads": 6,
    "hidden_units": 512,
    "n_encoding_layers": 3,
    "epochs": 20,
    "fine_tune": False,
    "optimizer": "adam",
    "target_size": 2,
    "skip_grams": 0,
    "hierarchical_softmax": 1,
    "embedding_window": 5,
    "min_count": 3,
    "embedding_iterations": 5,
    "scalling_coordinates": True,
    "choose_atention": "last"
}

config_submission_3 = {
    "batch_size": 128,
    "seq_len": 25,
    "seq_stride": 5,
    "d_model": 300,
    "heads": 6,
    "hidden_units": 512,
    "n_encoding_layers": 5,
    "epochs": 20,
    "fine_tune": False,
    "optimizer": "adam",
    "target_size": 2,
    "skip_grams": 1,
    "hierarchical_softmax": 1,
    "embedding_window": 9,
    "min_count": 3,
    "embedding_iterations": 10,
    "scalling_coordinates": True,
    "choose_atention": "last"
}
# Best weights 
# D:\projects\FMI Master\Practical ML\pml-2020-german-tweets\logs\transformer_25_56_300\1\best.pth

json_path = '../running_config.json'
run_env = 'python'
preprocess_script = 'preprocess.py'
training_script = 'train.py'
inference_script = 'inference.py'

def main():
    with open(json_path, 'w') as fout:
        json.dump(config_submission_2, fout)

    preprocess_cmd = f'{run_env} {preprocess_script} --input-json {json_path}'
    train_cmd = f'{run_env} {training_script} --input-json {json_path}'
    inference_cmd = f'{run_env} {inference_script} --input-json {json_path}'

    subprocess.check_call(preprocess_cmd, shell = True)
    subprocess.check_call(train_cmd, shell = True)
    subprocess.check_call(inference_cmd, shell = True)


if __name__ == "__main__": 
    main()


######### Embedder.py

import torch
import torch.nn as nn

class Embedder(nn.Module):
    """ embedder layer that uses gensim vectors
    """
    def __init__(self, embedder_path):
        super().__init__()
        self.embedder_path = embedder_path
        
        embedded_voc = torch.load(embedder_path)
        self.embeddings = nn.Embedding.from_pretrained(embedded_voc, freeze=True)
    
    def forward(self, x):
        x = self.embeddings(x)
        return x

######### EncoderLayer.py

import torch.nn as nn
import torch.nn.functional as F

from Norm import Norm
from MultiHeadAttention import MultiHeadAttention

class EncoderLayer(nn.Module):
    """ the greatest rectangle from the architecture figure - transformer layer
    """
    def __init__(self, d_model, heads, hidden_units=256, dropout_1=0.1, dropout_2=0.1, dropout_3=0.1):
        super().__init__()
        self.norm_1 = Norm(d_model)
        self.norm_2 = Norm(d_model)
        self.attn = MultiHeadAttention(heads, d_model)
        
        self.linear_1 = nn.Linear(d_model, hidden_units)
        self.linear_2 = nn.Linear(hidden_units, d_model)
        
        self.dropout_1 = nn.Dropout(dropout_1)
        self.dropout_2 = nn.Dropout(dropout_2)
        self.dropout_3 = nn.Dropout(dropout_3)
        
    def forward(self, x, mask=None):
        x_normed = self.norm_1(x)
        x_attention = self.attn(x_normed, x_normed , x_normed, mask)
        x = x + self.dropout_1(x_attention)
        
        x_normed = self.norm_2(x)
        x_normed = self.linear_1(x_normed)
        x_normed = self.dropout_2(F.relu(x_normed))
        x_normed = self.linear_2(x_normed)
        x = x + self.dropout_3(x_normed)
        return x

######### GermanTweetsDataset.py

from torch.utils.data import Dataset
import torch
import pandas as pd

class GermanTweetsDataset(Dataset):
    """ dataset class used just for training
    """
    def __init__(self, csv_path):
        super().__init__()
        self.path = csv_path
        self.df = pd.read_csv(csv_path)

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        row = self.df.iloc[idx]
        text = [int(x) for x in row['tweet'].split(' ')]
        text = torch.tensor(text)
        position = int(row['tweet_offset'])
        position = torch.tensor(position)
        
        latitude = row['latitude']
        longitude = row['longitude']
        labels = torch.tensor([latitude, longitude])
        
        return text, position, labels

######### GermanTweetsTestDataset.py

from torch.utils.data import Dataset
import torch
import pandas as pd

class GermanTweetsTestDataset(Dataset):
    """ dataset class used just for submition
    """
    def __init__(self, csv_path):
        super().__init__()
        self.path = csv_path
        self.grouped_df = pd.read_csv(csv_path).groupby(["tweet_id"])
        self.tweet_ids = list(self.grouped_df.groups.keys())

    def __len__(self):
        return len(self.tweet_ids)

    def __getitem__(self, idx):
        tweet_id = self.tweet_ids[idx]
        tweet_sequences = self.grouped_df.get_group(tweet_id)

        texts = []
        for row in tweet_sequences["tweet"].tolist():
            text = torch.tensor([int(x) for x in row.split(' ')])
            text = text.unsqueeze(0)
            texts.append(text)
        texts = torch.cat(texts)

        positions = tweet_sequences["tweet_offset"].tolist()
        positions = list(map(int, positions))
        positions = torch.tensor(positions)
        
        return tweet_id, texts, positions

######### GermanTweetsValidationDataset.py

from torch.utils.data import Dataset
import torch
import pandas as pd
import pdb

class GermanTweetsValidationDataset(Dataset):
    """ dataset class used just for plots regarding the validation dataset
    """
    def __init__(self, csv_path):
        super().__init__()
        self.path = csv_path
        self.grouped_df = pd.read_csv(csv_path).groupby(["tweet_id"])
        self.tweet_ids = list(self.grouped_df.groups.keys())

    def __len__(self):
        return len(self.tweet_ids)

    def __getitem__(self, idx):
        tweet_id = self.tweet_ids[idx]
        tweet_sequences = self.grouped_df.get_group(tweet_id)

        texts = []
        labels = []
        for row in tweet_sequences["tweet"].tolist():
            text = torch.tensor([int(x) for x in row.split(' ')])
            text = text.unsqueeze(0)
            texts.append(text)
        texts = torch.cat(texts)
        row = tweet_sequences.iloc[0]
        labels = torch.tensor([row['latitude'], row['longitude']])

        positions = tweet_sequences["tweet_offset"].tolist()
        positions = list(map(int, positions))
        positions = torch.tensor(positions)
        
        return tweet_id, texts, positions, labels

######### inference.py


from train import load_weights
from __init__ import get_localizer, get_device, testing_data_path, training_data_path, \
                     submission_path, scalling_params_path, get_device, hyper_graph_path, \
                     validation_data_path

from GermanTweetsTestDataset import GermanTweetsTestDataset
from GermanTweetsValidationDataset import GermanTweetsValidationDataset
from torch.utils.tensorboard import SummaryWriter

import torch
import torch.nn as nn
import torch.nn.functional as F
import pandas as pd
import numpy as np
import os
import pdb
import matplotlib.pyplot as plt
import argparse
import json
import pdb


def submit_test(localizer, scalling_coordinates):
    testing_dataset = GermanTweetsTestDataset(testing_data_path)

    test_df = pd.DataFrame()
    device = get_device()
    for idx, (tweet_id, sequences, positions) in enumerate(testing_dataset):
        with torch.set_grad_enabled(False):
            sequences = sequences.to(device)
            positions = positions.to(device)
            output = localizer(sequences, positions)
            result = torch.mean(output, axis=0)
            test_df = test_df.append(pd.Series(data={
                'id': int(tweet_id),
                'latitude': result[0].item(),
                'longitude': result[1].item()
            }, name=tweet_id))
            print(f'Inference progress {idx}/{len(testing_dataset)}', end='\r')

    scalling_params = np.load(scalling_params_path)
    scalling_param_names = list(scalling_params[:, 0])
    scalling_param_values = list(map(float, scalling_params[:, 1]))

    test_df[scalling_param_names] = scalling_param_values
    for coordonate in ['latitude', 'longitude']:
        if scalling_coordinates:
            test_df[coordonate] = test_df[f'{coordonate}_min'] + test_df[coordonate] * \
                                 (test_df[f'{coordonate}_max'] - test_df[f'{coordonate}_min'])
        else:
            test_df[coordonate] += test_df[f'{coordonate}_mean']
    for col in scalling_param_names:
        test_df.pop(col)

    test_df['id'] = test_df['id'].astype(int)
    test_df.rename(columns = {
        'latitude': 'lat',
        'longitude': 'long'
    }, inplace=True) 

    test_df.set_index('id', drop=True)
    test_df.to_csv(submission_path, index=False, float_format='%.15f')

def visualize_validation(localizer, exp_w):
    # plot training
    exp_dir = os.path.dirname(exp_w)
    history_path = os.path.join(exp_dir, 'history.csv')
    plot_path = os.path.join(exp_dir, 'training_plot.png')
    metrics = pd.read_csv(history_path)
    plt.figure(figsize=(25, 20))
    plt.plot(metrics["train_loss"])
    plt.plot(metrics["val_loss"])
    plt.legend(["train_loss", "val_loss"])
    plt.savefig(plot_path)


def visualize_errors(localizer, exp_w, scalling_coordinates):
    # plot the histogram of errors
    validation_dataset = GermanTweetsValidationDataset(validation_data_path)
    device = get_device()
    results_lat = []
    results_long = []

    mae_list = []
    mse_list = []

    if scalling_coordinates:
        scalling_params = np.load(scalling_params_path)
        scalling_param_values = list(map(float, scalling_params[:, 1]))
        coordonate_min = torch.tensor([scalling_param_values[0], scalling_param_values[2]]).to(device)
        coordonate_max = torch.tensor([scalling_param_values[1], scalling_param_values[3]]).to(device)

    for idx, (_, sequences, positions, labels) in enumerate(validation_dataset):
        with torch.set_grad_enabled(False):
            sequences = sequences.to(device)
            positions = positions.to(device)
            labels = labels.to(device)
            output = localizer(sequences, positions)
            result = torch.mean(output, axis=0)

            if scalling_coordinates:
                result = coordonate_min + result * (coordonate_max - coordonate_min)
                labels = coordonate_min + labels * (coordonate_max - coordonate_min)

            mae_list.append(F.l1_loss(result, labels, reduction='mean').item())
            mse_list.append(F.mse_loss(result, labels, reduction='mean').item())
            result_dif = result - labels

            results_lat.append(result_dif[0].item())
            results_long.append(result_dif[1].item())
            print(f'Validation progress {idx}/{len(validation_dataset)}', end='\r')
    
    print()
    exp_dir = os.path.dirname(exp_w)
    plot_path = os.path.join(exp_dir, 'errors_hist.png')
    plt.figure(figsize=(40, 20))
    plt.hist(results_lat, bins=60, alpha=.5, color='blue')
    plt.hist(results_long, bins=60, alpha=.5, color='red')
    plt.title("Histogram of errors")
    plt.legend(["latitude", "longitude"])
    plt.savefig(plot_path)
    print(f'Validation finished wiht MAE {np.mean(mae_list)} and RMSE {np.sqrt(np.mean(mse_list))}')


def log_hyperparams(config, validation_loss):
    """ adds the experiment result to the hyperparameter graph
    """
    with SummaryWriter() as writer:
        hyper_dict = {
            "seq_len": config["seq_len"],
            "seq_stride": config["seq_stride"],
            "d_model": config["d_model"],
            "heads": config["heads"],
            "hidden_units": config["hidden_units"],
            "n_encoding_layers": config["n_encoding_layers"],
            "embedding_window": config["embedding_window"],
            "min_count": config["min_count"],
            "embedding_iterations": config["embedding_iterations"],
            "scalling_coordinates": config["scalling_coordinates"],
            "choose_atention": config["choose_atention"]
        }
        hyper_loss = {
            "loss": validation_loss
        }
        writer.add_hparams(hyper_dict, hyper_loss)

def main(input_json):
    with open(input_json, 'r') as fin:
        config = json.load(fin)

    device = get_device()
    localizer = get_localizer(config)
    exp_w = config['experiment_weights']
    validation_loss = load_weights(localizer, exp_w)
    log_hyperparams(config, validation_loss)
    localizer = localizer.to(device)
    
    scalling_coordinates = config['scalling_coordinates']
    visualize_validation(localizer, exp_w)
    visualize_errors(localizer, exp_w, scalling_coordinates)
    submit_test(localizer, scalling_coordinates)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-json', dest='input_json', type=str, \
                        help='The file name of the json from where we\'ll process the data')
    args = parser.parse_args()
    args = vars(args)
    main(args['input_json'])

######### Localizer.py

import torch.nn as nn
from copy import deepcopy

from Embedder import Embedder
from PositionalEncoder import PositionalEncoder
from EncoderLayer import EncoderLayer
from Norm import Norm

def repeat_modules(module, n_encoding_layers):
    return nn.Sequential(*[deepcopy(module) for _ in range(n_encoding_layers)])

class Localizer(nn.Module):
    """ the complete model
    """
    def __init__(self, n_encoding_layers, embedder_path, d_model, heads, hidden_units, target_size):
        super().__init__()
        self.N = n_encoding_layers
        self.embedder = Embedder(embedder_path)
        self.pe = PositionalEncoder(d_model)
        
        encoder_layer = EncoderLayer(d_model, heads, hidden_units)
        self.encoder_layers = repeat_modules(encoder_layer, n_encoding_layers)
        self.norm = Norm(d_model)
        self.linear = nn.Linear(d_model, target_size)
    
    def forward(self, x, pos, mask=None):
        x = self.embedder(x)
        x = self.pe(x, pos)
        x = self.encoder_layers(x)
        x = self.norm(x)
        x = self.linear(x)
        final_state = x[:, -1, :]
        return final_state

######### MultiHeadAttention.py

import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

class MultiHeadAttention(nn.Module):
    def __init__(self, heads, d_model, dropout=0.1):
        super().__init__()
        
        self.heads = heads
        self.d_model = d_model
        self.d_k = d_model // heads
        
        self.q_linear = nn.Linear(d_model, d_model)
        self.v_linear = nn.Linear(d_model, d_model)
        self.k_linear = nn.Linear(d_model, d_model)
        self.dropout = nn.Dropout(dropout)
        self.out = nn.Linear(d_model, d_model)
        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        
    def get_nopeak_mask(self, seq_len):
        """ left to right attention mask
        """
        nopeak_mask = np.triu(np.ones((1, seq_len, seq_len)), k=1) \
                             .astype('uint8')
        nopeak_mask = torch.from_numpy(nopeak_mask == 0)
        return nopeak_mask.to(self.device)
        
    def attention(self, q, k, v, mask=None):
        """ applies mask on the attention result
        """
        scores = torch.matmul(q, k.transpose(-2, -1)) / np.sqrt(self.d_k)

        if mask is not None:
            mask = mask.unsqueeze(1)
            scores = scores.masked_fill(mask == 0, -1e9)
            scores = F.softmax(scores, dim=-1)

        if self.dropout is not None:
            scores = self.dropout(scores)

        output = torch.matmul(scores, v)
        return output
    
    def forward(self, q, k, v, mask=None):
        """ split the embedding vectors into self.heads parts
            and transposes the tensor to have the final dimension:
            batch_size * n_heads * seq_len * d_model
            
            if the mask is not defined, it will be given a no peak one that
            will allow the atention to work between a word and another from its
            past (but not the future ones)
            
            also brings the tensor to the input dimenstion
        """
        batch_size = q.size(0)
        
        k = self.k_linear(k).view(batch_size, -1, self.heads, self.d_k)
        k = k.transpose(1, 2)
        
        q = self.q_linear(q).view(batch_size, -1, self.heads, self.d_k)
        q = q.transpose(1, 2)
        
        v = self.v_linear(v).view(batch_size, -1, self.heads, self.d_k)
        v = v.transpose(1, 2)
        
        if mask is None:
            seq_len = q.size(2)
            mask = self.get_nopeak_mask(seq_len)
        
        scores = self.attention(q, k, v, mask)
        concat = scores.transpose(1, 2).contiguous() \
                       .view(batch_size, -1, self.d_model)
        output = self.out(concat)
        return output

######### Norm.py

import torch
import torch.nn as nn
import torch.nn.functional as F

class Norm(nn.Module):
    """ performs Layer Normalization
    """
    def __init__(self, d_model, eps=1e-6):
        super().__init__()
    
        self.size = d_model
        self.alpha = nn.Parameter(torch.ones(self.size))
        self.beta = nn.Parameter(torch.zeros(self.size))
        self.eps = eps
    def forward(self, x):
        x_mean = x.mean(dim=-1, keepdim=True)
        x_std = x.std(dim=-1, keepdim=True) + self.eps
        
        norm = self.alpha * (x - x_mean) / x_std + self.beta
        return norm

######### PositionalEncoder.py

import torch
import torch.nn as nn
import numpy as np

class PositionalEncoder(nn.Module):
    """ computes the positional encoder and stores inside the class
    """
    def __init__(self, d_model, max_seq_len=250):
        super().__init__()
        self.d_model = d_model
        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        
        pe = torch.zeros(max_seq_len, d_model)
        for pos in range(max_seq_len):
            for i in range(0, d_model, 2):
                pe[pos, i] = np.sin(pos / (10000 ** ((2 * i) / d_model)))
                pe[pos, i + 1] = np.cos(pos / (10000 ** ((2 * (i + 1)) /d_model)))
        
        self.pe = pe.to(self.device)
    
    def get_positionals(self, positions, seq_len):
        positionals = []
        for pos in positions:
            positional = self.pe[pos: pos + seq_len, :] \
                             .clone() \
                             .detach() \
                             .requires_grad_(False)
            positional = positional.unsqueeze(0)
            positionals.append(positional)
        positionals = torch.cat(positionals)
        return positionals
    
    
    def forward(self, x, positions):
        x = x * np.sqrt(self.d_model)
        seq_len = x.size(1)
        
        positionals = self.get_positionals(positions, seq_len)
        x = x + positionals
        return x

######### preprocess.py

import pandas as pd
import numpy as np
import re
import torch
import argparse
import json

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.snowball import GermanStemmer

from gensim.models import Word2Vec
import emot

from __init__ import word2vec_path, embedder_path, scalling_params_path, \
                     training_data_path, validation_data_path, testing_data_path, \
                     training_dataset, validation_dataset, testing_dataset

special_chars_re = r"[#\?\.$%_\[\]()+-:*\"!„“]"
single_letters_re = '\s\w\s'
spaces_re = r"\s+"

def convert_emoticons(text):
    """ turns the emoji and the emoticons into description between a pair of :
    """
    emojis = emot.emoji(text)
    for value, meaning in zip(emojis['value'], emojis['mean']):
        text = re.sub(value, f' {meaning} ', text, flags=re.I)
    
    emoticons = emot.EMOTICONS
    for emoticon in emoticons:
        text = re.sub(u'('+emoticon+')', \
                      "_".join(emoticons[emoticon].replace(",","").split()), \
                      text)
    return text

def char_cleaning(doc_str):
    """ erases special characters, apostrophe and places a user_tag word for any @
        also converts the emoticons and deletes single letter words
    """
    doc_str = re.sub(special_chars_re, ' ', doc_str, flags=re.I)
    doc_str = re.sub(r'‘', '_ ' , doc_str, flags=re.I)
    doc_str = re.sub(r'@\w+', ':user_tag:', doc_str, flags=re.I)
    doc_str = convert_emoticons(doc_str)
    
    # delete single letter words
    while re.search(single_letters_re, doc_str):
        doc_str = re.sub(single_letters_re, ' ' , doc_str, flags=re.I)
    doc_str = re.sub(spaces_re, ' ', doc_str)
    return doc_str.strip().lower()

def filter_stopwords(tweets_processed):
    """ gets rid of the stopwords using nltk 
    """
    german_stop_words = stopwords.words('german')

    filtered_tweets = []
    for tweet in tweets_processed:
        word_tokens = word_tokenize(tweet)
        filtered_tweet = [w for w in word_tokens if not w in german_stop_words]
        filtered_tweets.append(filtered_tweet)
    return filtered_tweets

def get_word_embedding(stemmed_tweets, training, config):
    """ trains a contextual word embedding which will not be further retrained
    """
    if training:
        german2vec = Word2Vec(
            sentences=stemmed_tweets, 
            sg=config["skip_grams"],
            hs=config["hierarchical_softmax"],
            size=config["d_model"], 
            window=config["embedding_window"],
            min_count=config["min_count"],
            iter=config["embedding_iterations"],
            workers=4
        )
        german2vec.save(word2vec_path)
    else:
        german2vec = Word2Vec.load(word2vec_path)
    return german2vec

def get_encoded_texts(german2vec, stemmed_tweets, seq_len):
    """ based on the vocabulary returned from word2vec, it computes 
        the embeddings list and saves it as torch.tensor;
        it also pads the incomplete sequences with a 0 hardcoded vector (blanc)
    """
    word_to_id = {}
    embedded_voc = []
    for idx, word in enumerate(german2vec.wv.vocab.keys()):
        word_to_id[word] = idx

        embedding = np.copy(german2vec.wv[word])
        embedding = torch.from_numpy(embedding)
        embedding = embedding.unsqueeze(0)
        embedded_voc.append(embedding)
        
        print(f'Preparation progress {idx}/{len(german2vec.wv.vocab)}', end='\r')
    print()

    # Add blanc words for completing the sequence
    blanc_id = len(german2vec.wv.vocab)
    embedded_voc.append(torch.zeros_like(embedding))

    embedded_voc = torch.cat(embedded_voc)
    torch.save(embedded_voc, embedder_path)

    tweets = []
    for tweet in stemmed_tweets:
        tweet = [word_to_id[word] for word in tweet if word in word_to_id.keys()]
        if len(tweet) < seq_len:
            blanc_spaces = (seq_len - len(tweet))
            tweet += blanc_spaces * [blanc_id]
        tweets.append(tweet)
    return tweets

def preprocess_tweets(dataset, training, config):
    """ performs the cleaning, stopwords removal, stemming, embedding;
        the embeddings return a vocabulary which is used for filtering the original text
        and then applying tf_idf algorithm; also, it appends the value 0 for the blanc word
    """
    tweets = list(dataset["tweet"])
    tweets_processed = list(map(char_cleaning, tweets))
    filtered_tweets = filter_stopwords(tweets_processed)

    stemmer = GermanStemmer()
    stemmed_tweets = [list(map(stemmer.stem, tweet)) for tweet in filtered_tweets]
    german2vec = get_word_embedding(stemmed_tweets, training, config)
    dataset["tweet"] = get_encoded_texts(german2vec, stemmed_tweets, \
                                         config['seq_len'])

    return dataset

def preprocess_coordinates(dataset, training, scalling_coordinates):
    """ performs the scalling of the gps coordinates or just the translation of the interval
    """
    scalling_cols = ['latitude', 'longitude']
    scalling_param_names = []
    scalling_param_values = []

    if training:
        for coordonate in scalling_cols:
            if scalling_coordinates:
                scalling_param_names.append(f'{coordonate}_min')
                scalling_param_values.append(dataset[coordonate].min())
                
                scalling_param_names.append(f'{coordonate}_max')
                scalling_param_values.append(dataset[coordonate].max())
            else:
                scalling_param_names.append(f'{coordonate}_mean')
                scalling_param_values.append(dataset[coordonate].mean())
    
        scalling_params = np.array(list(zip(scalling_param_names, scalling_param_values)))
        np.save(scalling_params_path, scalling_params)
    else:
        scalling_params = np.load(scalling_params_path)
        scalling_param_names = list(scalling_params[:, 0])
        scalling_param_values = list(map(float, scalling_params[:, 1]))

    dataset[scalling_param_names] = scalling_param_values

    for coordonate in scalling_cols:
        if scalling_coordinates:
            dataset[coordonate] = (dataset[coordonate] - dataset[f'{coordonate}_min']) / \
                                  (dataset[f'{coordonate}_max'] - dataset[f'{coordonate}_min'])
        else:
            dataset[coordonate] -= dataset[f'{coordonate}_mean']
    for col in scalling_param_names:
        dataset.pop(col)

    return dataset

def get_sequence_line(tweet_id, tweet_row, sequence, j, testing):
    """ returns a pandas series containing a line in the final dataframe;
        contains the sequence, tf-idf etc. 
    """
    line_config = {
        'tweet_id': int(tweet_id), 
        'tweet': " ".join(str(w_id) for w_id in sequence),
        'tweet_offset': j
    }
    if not testing:
        line_config.update({
            'latitude': tweet_row["latitude"], 
            'longitude': tweet_row["longitude"]
        })
                
    return pd.Series(data=line_config, name=f'{tweet_id}-{j}')

def get_sequence_dataset(dataset, seq_len, seq_stride, training, testing):
    """ turns the dataset into sequences having a length and a stride
    """
    seq_df = pd.DataFrame()

    try:
        for i in range(len(dataset)):
            tweet_row = dataset.iloc[i]
            tweet_id = dataset.index[i]
            
            tweet = tweet_row['tweet']
            for j in range(0, len(tweet) - seq_len + 1, seq_stride):
                sequence = tweet[j: j + seq_len]
                
                seq_line = get_sequence_line(tweet_id, tweet_row, sequence, j, testing)
                seq_df = seq_df.append(seq_line)
                
                print(f'Preparation progress {i}/{len(dataset)}', end='\r')
    except KeyboardInterrupt:
        print('\nIntreruption')
    finally:
        print('\nPreparation done')
        if training:
            seq_df.to_csv(training_data_path)
        elif testing:
            seq_df.to_csv(testing_data_path)
        else:
            seq_df.to_csv(validation_data_path)
    return seq_df

def export_sequenced_dataset(dataset_path, config, training=True, testing=False):
    """ performs the proper operations for the dataset:
        reading, the tweets, the coordinates, transformation into sequences
    """
    col_names = ['tweet-id', 'latitude','longitude', 'tweet']
    if testing:
        col_names = [col_names[0], col_names[-1]]

    dataset = pd.read_csv(dataset_path, names=col_names)
    dataset = dataset.set_index('tweet-id', drop=True)

    seq_len = config['seq_len']
    seq_stride = config['seq_stride']

    dataset = preprocess_tweets(dataset, training, config)
    if not testing:
        dataset = preprocess_coordinates(dataset, training, config["scalling_coordinates"])
    get_sequence_dataset(dataset, seq_len, seq_stride, training, testing)

def main(input_json):
    """ reads the json from calling master and preprocess the 3 datasets having the path in __init__
    """
    with open(input_json, 'r') as fin:
        config = json.load(fin)

    export_sequenced_dataset(training_dataset, config)
    export_sequenced_dataset(validation_dataset, config, training=False)
    export_sequenced_dataset(testing_dataset, config, training=False, testing=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-json', dest='input_json', type=str, \
                        help='The file name of the json from where we\'ll process the data')
    args = parser.parse_args()
    args = vars(args)
    main(args['input_json'])

######### train.py

from __init__ import get_localizer, training_data_path, validation_data_path, get_device

from GermanTweetsDataset import GermanTweetsDataset
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.optim import Adam

import torch
import torch.nn as nn

import numpy as np
import pandas as pd
import os
import json
import argparse

def get_lr(optim):
    """ extracts the learning rate from the optimizer
    """
    for param_group in optim.param_groups:
        return param_group['lr']

def training(dataloaders, localizer, optim, lr_scheduler, epochs, device, best_loss, exp_dir):
    """ performs the full training process with printing results and advancing with the lr_scheduler
    the try catch block saves the training scores inside the epochs, altough interupted
    """
    MAE = nn.L1Loss(reduction='mean')
    print_frequency = 10

    tensorboard_dir = os.path.join(exp_dir, 'runs/')
    writer = SummaryWriter(tensorboard_dir)
    print(f'Tensorboard is recording into folder: {tensorboard_dir}\n')

    try:
        results = []
        for epoch in range(epochs):
            for phase in ['train', 'test']:
                epoch_losses = []

                if phase == 'train':
                    # training mode
                    localizer.train()
                else:
                    # evaluate mode
                    localizer.eval()

                for i, batch in enumerate(dataloaders[phase]):
                    sequences, positions, labels = batch
                    sequences = sequences.to(device)
                    positions = positions.to(device)
                    labels = labels.to(device)

                    optim.zero_grad()
                    with torch.set_grad_enabled(phase == 'train'):
                        output = localizer(sequences, positions)
                        loss = MAE(output, labels)

                        if phase == 'train':
                            loss.backward()
                            optim.step()

                    epoch_losses.append(loss.item())
                    average_loss = np.mean(epoch_losses)
                    lr = get_lr(optim)

                    if (i + 1) % print_frequency == 0:
                        loading_percentage = int(100 * (i+1) / len(dataloaders[phase]))
                        print(f'{phase}ing epoch {epoch}, iter = {i+1}/{len(dataloaders[phase])} ' + \
                            f'({loading_percentage}%), loss = {loss}, average_loss = {average_loss} ' + \
                            f'learning rate = {lr}', end='\r')
                        
                        

                if phase == 'test' and average_loss < best_loss:
                    best_loss = average_loss
                    
                    torch.save({
                            'model': localizer.state_dict(),
                            'optimizer': optim.state_dict(),
                            'lr_scheduler': lr_scheduler.state_dict(),
                            'epoch': epoch,
                            'validation_loss': best_loss
                        }, os.path.join(exp_dir, 'best.pth'))
                    
                if phase == 'train':
                    metric_results = {
                        'train_loss': average_loss
                    }

                    writer.add_scalar('Train/Loss', average_loss, epoch)
                    writer.flush()

                if phase == 'test':
                    val_results = {
                        'val_loss': average_loss
                    }

                    metric_results.update(val_results)
                    results.append(metric_results)

                    writer.add_scalar('Test/Loss', average_loss, epoch)
                    writer.flush()

                    try:
                        lr_scheduler.step()
                    except:
                        lr_scheduler.step(average_loss)
                
                print()
    except Exception as ex:
        print(ex)
    finally:
        results = pd.DataFrame(results)
        history_path = os.path.join(exp_dir, 'history.csv')
        results.to_csv(history_path)
        return best_loss

def load_weights(model, path, optimizer=None, lr_scheduler=None):
    """ loads the customized dict of weights
    """
    sd = torch.load(path)
    model.load_state_dict(sd['model'])
    if optimizer:
        optimizer.load_state_dict(sd['optimizer'])
    if lr_scheduler:
        lr_scheduler.load_state_dict(sd['lr_scheduler'])
    epoch = sd['epoch']
    validation_loss = sd['validation_loss']

    print(f'Loaded model from epoch {epoch + 1} with validation loss = {validation_loss} \n')
    return validation_loss
 
def get_exp_dir(config, input_json):
    """ returns a new folder to export the model weigths and configuration
    """
    exp_dir = f'../logs/transformer_{config["seq_len"]}_{config["seq_stride"]}' + \
              f'{config["heads"]}_{config["d_model"]}'

    if config['fine_tune']:
        exp_dir += '_fine_tune'

    os.makedirs(exp_dir, exist_ok=True)

    experiments = [d for d in os.listdir(exp_dir) if os.path.isdir(os.path.join(exp_dir, d))]
    experiments = set(map(int, experiments))
    if len(experiments) > 0:
        possible_experiments = set(range(1, max(experiments) + 2))
        experiment_id = min(possible_experiments - experiments)
    else:
        experiment_id = 1

    exp_dir = os.path.join(exp_dir, str(experiment_id))
    os.makedirs(exp_dir, exist_ok=True)

    config_path = os.path.join(exp_dir, 'config.json')
    with open(config_path, 'w') as fout:
        json.dump(config, fout)

    exp_weights = os.path.join(exp_dir, 'best.pth')
    config["experiment_weights"] = exp_weights
    with open(input_json, 'w') as fout:
        json.dump(config, fout)
    return exp_dir

def main(input_json):
    with open(input_json, 'r') as fin:
        config = json.load(fin)

    batch_size = config["batch_size"]
    epochs = config["epochs"]
    device = get_device()

    localizer = get_localizer(config)
    localizer = localizer.to(device)
    training_dataset = GermanTweetsDataset(training_data_path)
    training_dataloader = DataLoader(training_dataset, batch_size=batch_size, \
                                     shuffle=True, num_workers=4)

    validation_dataset = GermanTweetsDataset(validation_data_path)
    validation_dataloader = DataLoader(validation_dataset, batch_size=batch_size, \
                                     shuffle=False, num_workers=4)

    dataloaders = {
        'train': training_dataloader,
        'test': validation_dataloader
    }

    if config["optimizer"] == "adam":
        optim = Adam(localizer.parameters(), lr=0.0001, betas=(0.9, 0.98), eps=1e-9)
    lr_scheduler = ReduceLROnPlateau(optim,
                                     factor = 0.1,
                                     patience = 3,
                                     mode = 'min')
    best_loss = 500
    exp_dir = get_exp_dir(config, input_json)
    best_loss = training(dataloaders, localizer, optim, lr_scheduler, epochs, device, best_loss, exp_dir)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-json', dest='input_json', type=str, \
                        help='The file name of the json from where we\'ll process the data')
    args = parser.parse_args()
    args = vars(args)
    main(args['input_json'])