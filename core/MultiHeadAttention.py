import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

class MultiHeadAttention(nn.Module):
    def __init__(self, heads, d_model, dropout=0.1):
        super().__init__()
        
        self.heads = heads
        self.d_model = d_model
        self.d_k = d_model // heads
        
        self.q_linear = nn.Linear(d_model, d_model)
        self.v_linear = nn.Linear(d_model, d_model)
        self.k_linear = nn.Linear(d_model, d_model)
        self.dropout = nn.Dropout(dropout)
        self.out = nn.Linear(d_model, d_model)
        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        
    def get_nopeak_mask(self, seq_len):
        """ left to right attention mask
        """
        nopeak_mask = np.triu(np.ones((1, seq_len, seq_len)), k=1) \
                             .astype('uint8')
        nopeak_mask = torch.from_numpy(nopeak_mask == 0)
        return nopeak_mask.to(self.device)
        
    def attention(self, q, k, v, mask=None):
        """ applies mask on the attention result
        """
        scores = torch.matmul(q, k.transpose(-2, -1)) / np.sqrt(self.d_k)

        if mask is not None:
            mask = mask.unsqueeze(1)
            scores = scores.masked_fill(mask == 0, -1e9)
            scores = F.softmax(scores, dim=-1)

        if self.dropout is not None:
            scores = self.dropout(scores)

        output = torch.matmul(scores, v)
        return output
    
    def forward(self, q, k, v, mask=None):
        """ split the embedding vectors into self.heads parts
            and transposes the tensor to have the final dimension:
            batch_size * n_heads * seq_len * d_model
            
            if the mask is not defined, it will be given a no peak one that
            will allow the atention to work between a word and another from its
            past (but not the future ones)
            
            also brings the tensor to the input dimenstion
        """
        batch_size = q.size(0)
        
        k = self.k_linear(k).view(batch_size, -1, self.heads, self.d_k)
        k = k.transpose(1, 2)
        
        q = self.q_linear(q).view(batch_size, -1, self.heads, self.d_k)
        q = q.transpose(1, 2)
        
        v = self.v_linear(v).view(batch_size, -1, self.heads, self.d_k)
        v = v.transpose(1, 2)
        
        if mask is None:
            seq_len = q.size(2)
            mask = self.get_nopeak_mask(seq_len)
        
        scores = self.attention(q, k, v, mask)
        concat = scores.transpose(1, 2).contiguous() \
                       .view(batch_size, -1, self.d_model)
        output = self.out(concat)
        return output