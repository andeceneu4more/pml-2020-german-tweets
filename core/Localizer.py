import torch.nn as nn
from copy import deepcopy

from Embedder import Embedder
from PositionalEncoder import PositionalEncoder
from EncoderLayer import EncoderLayer
from Norm import Norm

def repeat_modules(module, n_encoding_layers):
    return nn.Sequential(*[deepcopy(module) for _ in range(n_encoding_layers)])

class Localizer(nn.Module):
    """ the complete model
    """
    def __init__(self, n_encoding_layers, embedder_path, d_model, heads, hidden_units, target_size):
        super().__init__()
        self.N = n_encoding_layers
        self.embedder = Embedder(embedder_path)
        self.pe = PositionalEncoder(d_model)
        
        encoder_layer = EncoderLayer(d_model, heads, hidden_units)
        self.encoder_layers = repeat_modules(encoder_layer, n_encoding_layers)
        self.norm = Norm(d_model)
        self.linear = nn.Linear(d_model, target_size)
    
    def forward(self, x, pos, mask=None):
        x = self.embedder(x)
        x = self.pe(x, pos)
        x = self.encoder_layers(x)
        x = self.norm(x)
        x = self.linear(x)
        final_state = x[:, -1, :]
        return final_state