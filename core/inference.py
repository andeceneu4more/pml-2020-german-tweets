
from train import load_weights
from __init__ import get_localizer, get_device, testing_data_path, training_data_path, \
                     submission_path, scalling_params_path, get_device, hyper_graph_path, \
                     validation_data_path

from GermanTweetsTestDataset import GermanTweetsTestDataset
from GermanTweetsValidationDataset import GermanTweetsValidationDataset
from torch.utils.tensorboard import SummaryWriter

import torch
import torch.nn as nn
import torch.nn.functional as F
import pandas as pd
import numpy as np
import os
import pdb
import matplotlib.pyplot as plt
import argparse
import json
import pdb


def submit_test(localizer, scalling_coordinates):
    testing_dataset = GermanTweetsTestDataset(testing_data_path)

    test_df = pd.DataFrame()
    device = get_device()
    for idx, (tweet_id, sequences, positions) in enumerate(testing_dataset):
        with torch.set_grad_enabled(False):
            sequences = sequences.to(device)
            positions = positions.to(device)
            output = localizer(sequences, positions)
            result = torch.mean(output, axis=0)
            test_df = test_df.append(pd.Series(data={
                'id': int(tweet_id),
                'latitude': result[0].item(),
                'longitude': result[1].item()
            }, name=tweet_id))
            print(f'Inference progress {idx}/{len(testing_dataset)}', end='\r')

    scalling_params = np.load(scalling_params_path)
    scalling_param_names = list(scalling_params[:, 0])
    scalling_param_values = list(map(float, scalling_params[:, 1]))

    test_df[scalling_param_names] = scalling_param_values
    for coordonate in ['latitude', 'longitude']:
        if scalling_coordinates:
            test_df[coordonate] = test_df[f'{coordonate}_min'] + test_df[coordonate] * \
                                 (test_df[f'{coordonate}_max'] - test_df[f'{coordonate}_min'])
        else:
            test_df[coordonate] += test_df[f'{coordonate}_mean']
    for col in scalling_param_names:
        test_df.pop(col)

    test_df['id'] = test_df['id'].astype(int)
    test_df.rename(columns = {
        'latitude': 'lat',
        'longitude': 'long'
    }, inplace=True) 

    test_df.set_index('id', drop=True)
    test_df.to_csv(submission_path, index=False, float_format='%.15f')

def visualize_validation(localizer, exp_w):
    # plot training
    exp_dir = os.path.dirname(exp_w)
    history_path = os.path.join(exp_dir, 'history.csv')
    plot_path = os.path.join(exp_dir, 'training_plot.png')
    metrics = pd.read_csv(history_path)
    plt.figure(figsize=(25, 20))
    plt.plot(metrics["train_loss"])
    plt.plot(metrics["val_loss"])
    plt.legend(["train_loss", "val_loss"])
    plt.savefig(plot_path)


def visualize_errors(localizer, exp_w, scalling_coordinates):
    # plot the histogram of errors
    validation_dataset = GermanTweetsValidationDataset(validation_data_path)
    device = get_device()
    results_lat = []
    results_long = []

    mae_list = []
    mse_list = []

    if scalling_coordinates:
        scalling_params = np.load(scalling_params_path)
        scalling_param_values = list(map(float, scalling_params[:, 1]))
        coordonate_min = torch.tensor([scalling_param_values[0], scalling_param_values[2]]).to(device)
        coordonate_max = torch.tensor([scalling_param_values[1], scalling_param_values[3]]).to(device)

    for idx, (_, sequences, positions, labels) in enumerate(validation_dataset):
        with torch.set_grad_enabled(False):
            sequences = sequences.to(device)
            positions = positions.to(device)
            labels = labels.to(device)
            output = localizer(sequences, positions)
            result = torch.mean(output, axis=0)

            if scalling_coordinates:
                result = coordonate_min + result * (coordonate_max - coordonate_min)
                labels = coordonate_min + labels * (coordonate_max - coordonate_min)

            mae_list.append(F.l1_loss(result, labels, reduction='mean').item())
            mse_list.append(F.mse_loss(result, labels, reduction='mean').item())
            result_dif = result - labels

            results_lat.append(result_dif[0].item())
            results_long.append(result_dif[1].item())
            print(f'Validation progress {idx}/{len(validation_dataset)}', end='\r')
    
    print()
    exp_dir = os.path.dirname(exp_w)
    plot_path = os.path.join(exp_dir, 'errors_hist.png')
    plt.figure(figsize=(40, 20))
    plt.hist(results_lat, bins=60, alpha=.5, color='blue')
    plt.hist(results_long, bins=60, alpha=.5, color='red')
    plt.title("Histogram of errors")
    plt.legend(["latitude", "longitude"])
    plt.savefig(plot_path)
    print(f'Validation finished wiht MAE {np.mean(mae_list)} and RMSE {np.sqrt(np.mean(mse_list))}')


def log_hyperparams(config, validation_loss):
    """ adds the experiment result to the hyperparameter graph
    """
    with SummaryWriter() as writer:
        hyper_dict = {
            "seq_len": config["seq_len"],
            "seq_stride": config["seq_stride"],
            "d_model": config["d_model"],
            "heads": config["heads"],
            "hidden_units": config["hidden_units"],
            "n_encoding_layers": config["n_encoding_layers"],
            "embedding_window": config["embedding_window"],
            "min_count": config["min_count"],
            "embedding_iterations": config["embedding_iterations"],
            "scalling_coordinates": config["scalling_coordinates"],
            "choose_atention": config["choose_atention"]
        }
        hyper_loss = {
            "loss": validation_loss
        }
        writer.add_hparams(hyper_dict, hyper_loss)

def main(input_json):
    with open(input_json, 'r') as fin:
        config = json.load(fin)

    device = get_device()
    localizer = get_localizer(config)
    exp_w = config['experiment_weights']
    validation_loss = load_weights(localizer, exp_w)
    log_hyperparams(config, validation_loss)
    localizer = localizer.to(device)
    
    scalling_coordinates = config['scalling_coordinates']
    visualize_validation(localizer, exp_w)
    visualize_errors(localizer, exp_w, scalling_coordinates)
    submit_test(localizer, scalling_coordinates)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-json', dest='input_json', type=str, \
                        help='The file name of the json from where we\'ll process the data')
    args = parser.parse_args()
    args = vars(args)
    main(args['input_json'])