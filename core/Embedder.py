import torch
import torch.nn as nn

class Embedder(nn.Module):
    """ embedder layer that uses gensim vectors
    """
    def __init__(self, embedder_path):
        super().__init__()
        self.embedder_path = embedder_path
        
        embedded_voc = torch.load(embedder_path)
        self.embeddings = nn.Embedding.from_pretrained(embedded_voc, freeze=True)
    
    def forward(self, x):
        x = self.embeddings(x)
        return x